(*** Une première implantation des graphes ***)

module type Graph = 
sig

  type node

  module NodeSet : Set.S with type elt = node (*** on crée l'nesemble de noeuds de type node ***)

  module NodeMap : Map.S with type key = node 

  type graph

  val empty : graph

  val is_empty : graph -> bool

  val equal : graph -> graph -> bool

  val add_vertex : node -> graph -> graph

  val add_edge : node -> node -> graph -> graph

  val succs : node -> graph -> NodeSet.t

  val nb_vertex : graph -> int

  val nb_edge : graph -> int

  val degree : node -> graph -> int

  val max_degree : graph -> (int * node)

  val transpose : graph -> graph

  val fold_vertex : (node -> 'a -> 'a) -> graph -> 'a -> 'a

  val fold_edge : (node -> node -> 'a -> 'a) -> graph -> 'a -> 'a

  val union : graph -> graph -> graph

  val inter : graph -> graph -> graph

  val complement : graph -> graph

end

module Make (N:Set.OrderedType) = 
struct

  type node = N.t

  module NodeSet = Set.Make(N) (*** On crée l'ensemble de noeuds avec le module Set ***)

  module NodeMap = Map.Make(N) (*** On crée une table d'association ayant pour clé le type passé en paramètre avec le module Map ***)

  type graph = NodeSet.t NodeMap.t  (***  On a le type graphe a qui un noeud est associé à un ensemble de noeuds ***)
  
  let empty = NodeMap.empty (*** La table d'association vide ***)

  let is_empty = NodeMap.is_empty

  let equal = NodeMap.equal NodeSet.equal

  let add_vertex v g = 
    if NodeMap.mem v g
    then g
    else NodeMap.add v NodeSet.empty g

  let add_edge v1 v2 g = 
    let g' = add_vertex v1 (add_vertex v2 g) in
    let succs = NodeMap.find v1 g' in
    NodeMap.add v1 (NodeSet.add v2 succs) g'

  let succs v g = NodeMap.find v g (*** retourne ce qui est associé au noued v dans le graphe g (ensemble de noeuds) ***)

  let nb_vertex = NodeMap.cardinal

  let degree v g = NodeSet.cardinal (NodeMap.find v g)
 
  let fold_vertex f g v0 = 
    NodeMap.fold (fun a _ b -> f a b) g v0
  
  let fold_edge f g x =
    NodeMap.fold 
      (fun v1 succs acc ->
         NodeSet.fold
           (fun v2 acc -> f v1 v2 acc)
           succs acc)
      g x

  let nb_edge g = fold_vertex (fun v acc -> acc + (NodeSet.cardinal (NodeMap.find v g))) g 0

  let max_degree g =
    fold_vertex 
      (fun v (max, v_max) -> 
         let deg = degree v g in
         if deg > max then (deg, v)
         else (max, v_max))
      g (-1, let a,_ = NodeMap.choose g in a)

  let transpose g = fold_edge
      (fun v1 v2 acc -> add_edge v2 v1 acc) g empty
  
  let union = NodeMap.union (fun _ s1 s2 -> Some (NodeSet.union s1 s2))

  let inter = 
    NodeMap.merge (fun _ s1 s2 -> match s1, s2 with
        |None,_ -> None
        |_,None -> None
        |Some(a), Some(b) -> Some (NodeSet.inter a b))
    
  let complement g =
    let ens = NodeMap.fold (fun a _ acc -> NodeSet.add a acc) g NodeSet.empty in
    NodeMap.fold 
      (fun source succs acc ->
         NodeMap.add source
           (NodeSet.filter (fun elt -> not (NodeSet.mem elt succs)) ens)
           acc)
      g empty

end


module MakeBFS = functor(G: Graph) -> 
  struct 

    type 'a option = |Node |Some of 'a 

    exception Not_found

    type node = G.node
    
    module NodeSet = G.NodeSet (*** on crée l'nesemble de noeuds de type node ***)

    module NodeMap = G.NodeMap
   
    let rec getpath acc curr seen = 
      let pred = NodeMap.find curr seen in 
      if curr = pred then acc
      else getpath (curr::acc) pred seen 

    let rec bfs_aux seen todo next dst g = 
      match todo with 
      |[] -> (match next with                                    (*** Si [src dst] = [], on regarde si next est vide ou pas ***)
              |[] -> None
              |_ -> bfs_aux seen next [] dst g)
      |[n;p]::todo' -> if n = dst then Some (getpath [n] p seen)  (*** Si src = dst, on cherche le chemin enrte les noeuds (les mm) ***)
                       else if NodeMap.mem n seen 
                       then bfs_aux seen todo' next dst g 
                       else let seen' = NodeMap.add n p seen in (*** On ajoute le noeud n à ceux déjà vus ***)
                        let ms = G.succs n g in 
                        let next' = NodeSet.fold (fun m acc -> [m;n]::acc) ms next  (***  On ajoute chaque noeud de ms dans next ***)
                        in bfs_aux seen' todo' next' dst g 
                        
    let bfs src dst g = bfs_aux NodeMap.empty [src;dst] [] dst g 



    

    
  

  end 