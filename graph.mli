(*** Une interface des graphes ***)

module type Graph = 
sig

  type node

  module NodeSet : Set.S with type elt = node (*** on crée l'nesemble de noeuds de type node ***)

  module NodeMap : Map.S with type key = node 

  type graph

  val empty : graph

  val is_empty : graph -> bool

  val equal : graph -> graph -> bool

  val add_vertex : node -> graph -> graph

  val add_edge : node -> node -> graph -> graph

  val succs : node -> graph -> NodeSet.t

  val nb_vertex : graph -> int

  val nb_edge : graph -> int

  val degree : node -> graph -> int

  val max_degree : graph -> (int * node)

  val transpose : graph -> graph

  val fold_vertex : (node -> 'a -> 'a) -> graph -> 'a -> 'a

  val fold_edge : (node -> node -> 'a -> 'a) -> graph -> 'a -> 'a

  val union : graph -> graph -> graph

  val inter : graph -> graph -> graph

  val complement : graph -> graph

end

module Make(N: Set.OrderedType) : Graph with type node = N.t

module MakeBFS : functor(G: Graph) -> 
  sig 

    type 'a option = |Node |Some of 'a 

    type node = G.node

    (*val bfs : node -> node -> G.graph -> (node list)option*)

    val shortest_path_bfs: node -> node -> G.graph
       
  end 